from django.shortcuts import render, get_object_or_404
from gallery import *
from .models import Gallery

def foto_gallery(request):
    gallerys = Gallery.objects.all()
    return render(request, 'gallery/gallery.html', {'gallerys':gallerys})

def foto_foto(request, pk):
    foto = get_object_or_404(Gallery, pk=pk)
    return render(request, 'gallery/foto.html', {'foto':foto})


