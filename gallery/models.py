from django.db import models
from django.conf import settings
from django.utils import timezone
from blog.models import Tag

class Gallery(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField(max_length=200)
    published_date = models.DateTimeField(default=timezone.now)
    autor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, null=True)
    def __str__(self):
        return self.title

class Foto(models.Model):
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='gallery/')
    description = models.TextField(verbose_name='description/', null=True)
    def __str__(self):
        return self.gallery.title+'foto'