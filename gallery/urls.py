from . import views

from django.urls import path

app_name='gallery'

urlpatterns = [
    path('', views.foto_gallery, name='foto_gallery'),
    path('foto/<int:pk>', views.foto_foto, name='foto_foto'),
]
