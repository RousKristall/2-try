from django.contrib import admin
from .models import Gallery, Foto

class FotoInLine(admin.TabularInline):
    model= Foto

class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title','published_date')
    list_filter = ('published_date','autor','published_date')
    inlines = [FotoInLine,]

admin.site.register(Gallery, GalleryAdmin)