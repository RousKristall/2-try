from django.http import  HttpResponseRedirect
from django.shortcuts import render, reverse
from django.utils import timezone
from .models import Post
from .forms import NewPostForms
from .forms import NewPostForms

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})

def viv_post(request, pk):
    my_post = Post.objects.get(pk=pk)
    return render(request, 'blog/viv_post.html', {'post':my_post})

def add_post(request):
    if request.method == 'GET':
        form = NewPostForms().as_p()
        return render(request, 'blog/add_post.html', {"form":form})
    elif request.method=="POST":
        NewPostForms(request.POST).save()
        return HttpResponseRedirect(reverse("blog:add_post"))