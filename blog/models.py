from django.db import models

from django.conf import settings

from django.utils import timezone


class Tag(models.Model):
    name=models.CharField(max_length=20)

    def __str__(self):
        return self.name

class Post(models.Model):
    title=models.CharField(max_length=50)
    txt=models.TextField()
    #published_date=models.DateTimeField(auto_now=True)
    created_date = models.DateTimeField(blank=True, null=True)
    published_date=models.DateTimeField(default=timezone.now)
    autor=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tags=models.ManyToManyField(Tag, null=True)

    def publish(self):
        self.published_date=timezone.now()
        self.save()

    def __str__(self):
        return self.title