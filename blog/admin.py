from django.contrib import admin
from .models import Post, Tag

# Register your models here.
class TagInLine(admin.TabularInline):
    model= Tag

class Post(admin.ModelAdmin):
    list_display = ('title','autor','')
    inlines = [TagInLine,]
