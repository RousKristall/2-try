from . import views

from django.urls import path, include

app_name='blog'

urlpatterns = [
    path('', views.post_list),
    path('post/<int:pk>/', views.viv_post),
    path('post/add_post/', views.add_post, name='add_post'),
]
