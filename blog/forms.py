from django import forms
from .models import Post, Tag

class NewPostForms(forms.ModelForm):
    class Meta:
        fields='__all__'
        model=Post
